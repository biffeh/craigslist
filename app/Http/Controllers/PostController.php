<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;


class PostController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function forma () {
        return view('pages.forma');
    }
    public function saugok()
    {
        $this->validate(request(),
            [
                'title'=> 'required',
                'category'=>'required',
                'body'=> 'required'
            ]);
        Post::create([
            'title'=>request('title'),
            'category'=>request('category'),
            'body'=>request('body'),
            'user_id'=>auth()->id()
        ]);
        return redirect('/admin');
    }
    public function home() {
        return view('pages.home');
    }

    public function editPost(Post $post)
    {
        if (Gate::denies('edit-post',$post))
        {
            return view('pages.restrict');
        }
        return view('pages.edit-post',compact('post'));
    }
    public function updatePost(Request $request, Post $post)
    {

        Post::where('id',$post->id)->update($request->only(['title','category','body']));
        return redirect('/admin');
    }
    public function deletePost(Post $post)
    {
        if (Gate::denies('delete-post',$post))
        {
            return view('pages.restrict');
        }
        $post->delete();
        return redirect('/admin');
    }
    public function admin()
    {
//        $posts = Post::get(['title','category','user_id','body']);
//        $posts = DB::table('posts')->where('user_id',Auth::id())->get();
//        $post = Post::all();
//        $user = Post::leftJoin('users', 'users.id', '=', 'posts.user_id')->select('users.name')
//            ->get(['name']);
        $post = Post::leftJoin('users', 'users.id', '=', 'posts.user_id')->select('title','category','user_id','body','name','posts.id')->get();
        return view('pages.dashboard',compact('post'));
    }



}
