<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comment;
use App\Post;
use Illuminate\Support\Facades\Auth;

class CommentController extends Controller
{
    public function addComment(Post $post)
    {
//        $this->validate(request(),
//            [
//                'title'=> 'required',
//                'body'=> 'required'
//           ]);
        if (Auth::check())
        Comment::create(
            [
                'body' => request('body'),
                'post_id'=>$post->id,
                'user_id' =>auth()->id()
            ]);
        else
        Comment::create(
            [
            'body' => request('body'),
            'post_id'=>$post->id,
            'user_id' =>0,
        ]);
        return back();
    }
}
