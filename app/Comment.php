<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


/**
 * App\Comment
 *
 * @property int $id
 * @property string $body
 * @property int $post_id
 * @property int|null $user_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Post $comments
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Comment whereBody($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Comment whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Comment whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Comment wherePostId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Comment whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Comment whereUserId($value)
 * @mixin \Eloquent
 */
class Comment extends Model
{
     protected $fillable = ['user_id','post_id','body'];
//
//    protected $protected = ['updated_at', 'created_at'];

    public function comments()
    {
        return $this->belongsTo(Post::class);
    }
}
