@extends('layouts/main')
@section('content')
    {{--<div class="col-md-4">--}}
    {{--<h2>Heading</h2>--}}
    {{--<p>{{str_limit('Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui.', 100)}} </p>--}}
    {{--<p><a class="btn btn-default" href="#" role="button">Rodyti visą informaciją &raquo;</a></p>--}}
    {{--</div>--}}
    {{--<div class="col-md-4">--}}
    {{--<h2>Heading</h2>--}}
    {{--<p>{{str_limit('Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui.',100)}} </p>--}}
    {{--<p><a class="btn btn-default" href="#" role="button">Rodyti visą informaciją &raquo;</a></p>--}}
    {{--</div>--}}
    {{--<div class="col-md-4">--}}
    {{--<h2>Heading</h2>--}}
    {{--<p>{{str_limit('Donec sed odio dui. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula porta felis euismod semper. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.',100)}}</p>--}}
    {{--<p><a class="btn btn-default" href="#" role="button">Rodyti visą informaciją &raquo;</a></p>--}}
    {{--</div>--}}

    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Kategorijos<span class="caret"></span></a>
        <ul class="dropdown-menu">
            @foreach($posts as $post)
                <li><a href="{{$post->category}}">{{$post->category}}</a></li>
                <li role="separator" class="divider"></li>

            @endforeach
        </ul>
    </li>

    @foreach($posts as $post)
        <div class="col-md-4">
            <h2>{{$post->title}}</h2>
            <h3>{{$post->category}}</h3>
            <p>{{str_limit($post->body, 100)}}</p>
            <p><a class="btn btn-default" href="post/{{$post->id}}" role="button">Rodyti visą informaciją</a></p>
        </div>

    @endforeach

    {{$posts->links()}}
@endsection